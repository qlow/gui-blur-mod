package com.tterrag.blur.util;

import com.google.common.collect.ImmutableSet;
import com.tterrag.blur.Blur;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.client.resources.IResourceManagerReloadListener;
import net.minecraft.client.resources.IResourcePack;
import net.minecraft.client.resources.data.IMetadataSection;
import net.minecraft.client.resources.data.IMetadataSerializer;
import net.minecraft.client.resources.data.PackMetadataSection;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ResourceLocation;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * Class created by qlow | Jan
 */
public class ShaderResourcePack implements IResourcePack, IResourceManagerReloadListener {

    private final Map<ResourceLocation, String> loadedData = new HashMap<ResourceLocation, String>();

    @Override
    public void onResourceManagerReload( IResourceManager resourceManager ) {
        this.loadedData.clear();
    }

    protected boolean validPath( final ResourceLocation location ) {
        return location.getResourceDomain().equals( "minecraft" ) && location.getResourcePath().startsWith( "shaders/" );
    }

    @Override
    public InputStream getInputStream( ResourceLocation location ) throws IOException {
        if ( this.validPath( location ) ) {
            String s = loadedData.get( location );

            if ( s == null ) {
                final InputStream in = Blur.class.getResourceAsStream( "/" + location.getResourcePath() );
                final StringBuilder data = new StringBuilder();
                final Scanner scan = new Scanner( in );

                try {
                    while ( scan.hasNextLine() ) {
                        data.append( scan.nextLine().replaceAll( "@radius@", Integer.toString( Blur.getInstance().getRadius() ) ) ).append( '\n' );
                    }
                } finally {
                    scan.close();
                }

                s = data.toString();
                loadedData.put( location, s );
            }

            return new ByteArrayInputStream( s.getBytes() );
        }

        throw new FileNotFoundException( location.toString() );
    }

    @Override
    public boolean resourceExists( ResourceLocation location ) {
        return this.validPath( location ) && Blur.class.getResource( "/" + location.getResourcePath() ) != null;
    }

    @Override
    public Set<String> getResourceDomains() {
        return ImmutableSet.of( "minecraft" );
    }

    @Override
    public <T extends IMetadataSection> T getPackMetadata( IMetadataSerializer p_135058_1_, String p_135058_2_ ) throws IOException {
        return ( T ) new PackMetadataSection( new ChatComponentText( "Blur's default shaders" ), 3 );
    }

    @Override
    public BufferedImage getPackImage() throws IOException {
        throw new FileNotFoundException( "pack.png" );
    }

    @Override
    public String getPackName() {
        return "Blur dummy resource pack";
    }
}
