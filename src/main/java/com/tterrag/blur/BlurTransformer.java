package com.tterrag.blur;

import net.minecraft.launchwrapper.IClassTransformer;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.VarInsnNode;

/**
 * Class created by qlow | Jan
 */
public class BlurTransformer implements IClassTransformer {

    public BlurTransformer() {

    }

    @Override
    public byte[] transform( String name, String transformedName, byte[] basicClass ) {
        if ( name.equals( "axu" ) || name.equals( "net.minecraft.client.gui.GuiScreen" ) ) {
            System.out.println( "Transforming Class [" + transformedName + "], Method [drawWorldBackground]" );

            final ClassNode classNode = new ClassNode();
            final ClassReader classReader = new ClassReader( basicClass );
            classReader.accept( ( ClassVisitor ) classNode, 0 );

            for ( final MethodNode m : classNode.methods ) {
                if ( m.name.equals( "drawWorldBackground" ) || m.name.equals( "func_146270_b" ) || m.name.equals( "b_" ) ) {
                    for ( int i = 0; i < m.instructions.size(); ++i ) {
                        final AbstractInsnNode next = m.instructions.get( i );
                        if ( next.getOpcode() == Opcodes.LDC ) {
                            System.out.println( "Modifying GUI background darkness... " );
                            final AbstractInsnNode colorHook = new MethodInsnNode( Opcodes.INVOKESTATIC, "com/tterrag/blur/Blur", "getBackgroundColor", "(Z)I", false );
                            final AbstractInsnNode colorHook2 = colorHook.clone( null );
                            m.instructions.set( next, colorHook );
                            m.instructions.set( colorHook.getNext(), colorHook2 );
                            m.instructions.insertBefore( colorHook, new InsnNode( Opcodes.ICONST_1 ) );
                            m.instructions.insertBefore( colorHook2, new InsnNode( Opcodes.ICONST_0 ) );
                            break;
                        }
                    }
                    break;
                }
            }

            final ClassWriter cw = new ClassWriter( 1 );
            classNode.accept( cw );

            System.out.println( "Transforming " + transformedName + " Finished." );

            return cw.toByteArray();
        }

        if ( name.equals( "ave" ) || name.equals( "net.minecraft.client.Minecraft" ) ) {
            System.out.println( "Transforming Class [" + transformedName + "], Method [drawWorldBackground]" );

            final ClassNode classNode = new ClassNode();
            final ClassReader classReader = new ClassReader( basicClass );
            classReader.accept( ( ClassVisitor ) classNode, 0 );

            for ( final MethodNode m : classNode.methods ) {
                if ( (m.name.equals( "am" ) || m.name.equals( "func_71384_a" ) || m.name.equals( "startGame" )) && m.desc.equals( "()V" ) ) {

                    String lastListName = null;

                    for ( int i = 0; i < m.instructions.size(); ++i ) {
                        final AbstractInsnNode next = m.instructions.get( i );

                        if ( next instanceof FieldInsnNode && (( FieldInsnNode ) next).desc.equals( "Ljava/util/List;" ) ) {
                            lastListName = (( FieldInsnNode ) next).name;
                            continue;
                        }

                        if ( lastListName != null && next instanceof MethodInsnNode && (( MethodInsnNode ) next).name.equals( "add" ) ) {
                            InsnList toInsert = new InsnList();

                            toInsert.add( new VarInsnNode( Opcodes.ALOAD, 0 ) );
                            toInsert.add( new FieldInsnNode( Opcodes.GETFIELD, classNode.name, lastListName, "Ljava/util/List;" ) );
                            toInsert.add( new FieldInsnNode( Opcodes.GETSTATIC, "com/tterrag/blur/Blur", "dummyPack", "Lcom/tterrag/blur/util/ShaderResourcePack;" ) );
                            toInsert.add( new MethodInsnNode( Opcodes.INVOKEINTERFACE, "java/util/List", "add", "(Ljava/lang/Object;)Z", true ) );
                            toInsert.add( new InsnNode( Opcodes.POP ) );

                            m.instructions.insert( next, toInsert );
                            break;
                        }
                    }

                    break;
                }
            }

            final ClassWriter cw = new ClassWriter( 1 );
            classNode.accept( cw );

            System.out.println( "Transforming " + transformedName + " Finished." );

            return cw.toByteArray();

        }

        return basicClass;
    }

}
