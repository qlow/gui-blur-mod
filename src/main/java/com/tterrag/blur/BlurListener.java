package com.tterrag.blur;

import net.labymod.core.LabyModCore;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.shader.Shader;
import net.minecraft.client.shader.ShaderGroup;
import net.minecraft.client.shader.ShaderUniform;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Class created by qlow | Jan
 */
public class BlurListener {

    private Field _listShaders;
    private long start;

    @SubscribeEvent
    public void onGuiChange( final GuiOpenEvent event ) {
        if ( this._listShaders == null ) {
            this._listShaders = ReflectionHelper.findField( ShaderGroup.class, "d", "field_148031_d", "listShaders" );
        }
        if ( LabyModCore.getMinecraft().getWorld() != null ) {
            final EntityRenderer er = Minecraft.getMinecraft().entityRenderer;

            if ( !er.isShaderActive() && event.gui != null && !(!Blur.getInstance().isBlurInChat() && event.gui instanceof GuiChat) ) {
                Blur.getInstance().loadShader( new ResourceLocation( "shaders/post/fade_in_blur.json" ) );
                this.start = System.currentTimeMillis();
            } else if ( er.isShaderActive() && event.gui == null ) {
                er.stopUseShader();
            }
        }
    }

    @SubscribeEvent
    public void onRenderTick( final TickEvent.RenderTickEvent event ) {
        float progress = getProgress();

        if ( progress != 1 ) {
            if ( event.phase == TickEvent.Phase.START && Minecraft.getMinecraft().currentScreen != null && Minecraft.getMinecraft().entityRenderer.isShaderActive() ) {
                final ShaderGroup sg = Minecraft.getMinecraft().entityRenderer.getShaderGroup();

                try {
                    final List<Shader> shaders = ( List<Shader> ) this._listShaders.get( sg );

                    for ( final Shader s : shaders ) {
                        final ShaderUniform su = s.getShaderManager().getShaderUniform( "Progress" );

                        if ( su != null ) {
                            su.set( progress );
                        }
                    }

                } catch ( IllegalArgumentException ex ) {
                } catch ( IllegalAccessException ex2 ) {
                }
            }
        }
    }

    public float getProgress() {
        return Math.min( (( float ) (System.currentTimeMillis() - this.start)) / (( float ) Blur.getInstance().getFadeTime() * 100F), 1.0f );
    }

}
