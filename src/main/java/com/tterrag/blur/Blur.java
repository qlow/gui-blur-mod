package com.tterrag.blur;

import com.tterrag.blur.util.ShaderResourcePack;
import lombok.Getter;
import net.labymod.api.LabyModAddon;
import net.labymod.core.LabyModCore;
import net.labymod.gui.elements.ColorPicker;
import net.labymod.settings.elements.BooleanElement;
import net.labymod.settings.elements.ColorPickerCheckBoxBulkElement;
import net.labymod.settings.elements.ControlElement;
import net.labymod.settings.elements.SettingsElement;
import net.labymod.settings.elements.SliderElement;
import net.labymod.utils.Consumer;
import net.labymod.utils.Material;
import net.labymod.utils.ModColor;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.resources.SimpleReloadableResourceManager;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

import java.awt.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Class created by qlow | Jan
 */
@Getter
public class Blur extends LabyModAddon {

    @Getter
    private static Blur instance;

    public static ShaderResourcePack dummyPack = new ShaderResourcePack();

    private long fadeTime;
    private int radius;
    private boolean blurInChat;

    private int gradientStartColor;
    private int gradientStartAlpha;

    private int gradientEndColor;
    private int gradientEndAlpha;

    private BlurListener blurListener = new BlurListener();

    private Method loadShaderMethod;

    @Override
    public void onEnable() {
        instance = this;

        // Registering listeners
        getApi().registerForgeListener( blurListener );

        // Adding the dummy pack to minecraft's lists
        (( SimpleReloadableResourceManager ) Minecraft.getMinecraft().getResourceManager()).registerReloadListener( dummyPack );

        // Disabling fast render
        try {
            Field fastRenderField = ReflectionHelper.findField( GameSettings.class, "ofFastRender" );
            fastRenderField.set( Minecraft.getMinecraft().gameSettings, false );

            // Save option
            Minecraft.getMinecraft().gameSettings.saveOptions();

            // Update framebuffer
            Minecraft.getMinecraft().getFramebuffer().createBindFramebuffer( Minecraft.getMinecraft().displayWidth, Minecraft.getMinecraft().displayHeight );

            if ( Minecraft.getMinecraft().entityRenderer != null )
                Minecraft.getMinecraft().entityRenderer.updateShaderGroupSize( Minecraft.getMinecraft().displayWidth, Minecraft.getMinecraft().displayHeight );
        } catch ( Exception ex ) {
        }
    }

    @Override
    public void onDisable() {

    }

    @Override
    public void loadConfig() {
        // The time between opening a GUI and the full blur state
        this.fadeTime = !getConfig().has( "fadeTime" ) ? 5 : getConfig().get( "fadeTime" ).getAsInt();

        // The blur's radius
        int radius = !getConfig().has( "radius" ) ? 12 : getConfig().get( "radius" ).getAsInt();

        // Whether or not the blur should be visible in the chat
        this.blurInChat = getConfig().has( "blurInChat" ) && getConfig().get( "blurInChat" ).getAsBoolean();

        // The radius has been changed?
        if ( this.radius != radius ) {
            this.radius = radius;

            dummyPack.onResourceManagerReload( Minecraft.getMinecraft().getResourceManager() );

            if ( LabyModCore.getMinecraft().getWorld() != null ) {
                if ( Minecraft.getMinecraft().currentScreen == null ) {
                    Minecraft.getMinecraft().entityRenderer.stopUseShader();
                } else {
                    loadShader( new ResourceLocation( "shaders/post/fade_in_blur.json" ) );
                }
            }
        }

        // The gradient colors
        this.gradientStartColor = !getConfig().has( "gradientStartColor" ) ? Color.BLACK.getRGB() : getConfig().get( "gradientStartColor" ).getAsInt();
        this.gradientStartAlpha = !getConfig().has( "gradientStartAlpha" ) ? 115 : getConfig().get( "gradientStartAlpha" ).getAsInt();
        this.gradientEndColor = !getConfig().has( "gradientEndColor" ) ? Color.BLACK.getRGB() : getConfig().get( "gradientEndColor" ).getAsInt();
        this.gradientEndAlpha = !getConfig().has( "gradientEndAlpha" ) ? 115 : getConfig().get( "gradientEndAlpha" ).getAsInt();

    }

    @Override
    protected void fillSettings( List<SettingsElement> subSettings ) {
        subSettings.clear();

        // The fade time
        subSettings.add(
                new SliderElement( "Fade time (1/10 seconds)", this, new ControlElement.IconData( Material.WATCH ), "fadeTime", ( int ) this.fadeTime )
                        .setMinValue( 0 ).setMaxValue( 50 ).setSteps( 1 )
        );

        // The radius
        subSettings.add( new SliderElement( "Radius", this, new ControlElement.IconData( Material.REDSTONE ), "radius", this.radius ).setRange( 1, 100 ) );

        // Blur in chat?
        subSettings.add( new BooleanElement( "Blur in Chat", this, new ControlElement.IconData( Material.SIGN ), "blurInChat", this.blurInChat ) );

        // The color stuff
        ColorPickerCheckBoxBulkElement bulkElement = new ColorPickerCheckBoxBulkElement( "Gradient Colors" );

        ColorPicker startColorPicker = new ColorPicker( "G. Start", new Color( gradientStartColor ), new ColorPicker.DefaultColorCallback() {
            @Override
            public Color getDefaultColor() {
                return Color.BLACK;
            }
        }, 0, 0, 0, 0 );

        startColorPicker.setUpdateListener( new Consumer<Color>() {
            @Override
            public void accept( Color accepted ) {
                Blur.this.gradientStartColor = accepted.getRGB();
                getConfig().addProperty( "gradientStartColor", gradientStartColor );
            }
        } );

        startColorPicker.setHasAdvanced( true );

        ColorPicker endColorPicker = new ColorPicker( "G. End", new Color( gradientEndColor ), new ColorPicker.DefaultColorCallback() {
            @Override
            public Color getDefaultColor() {
                return Color.BLACK;
            }
        }, 0, 0, 0, 0 );

        endColorPicker.setUpdateListener( new Consumer<Color>() {
            @Override
            public void accept( Color accepted ) {
                Blur.this.gradientEndColor = accepted.getRGB();
                getConfig().addProperty( "gradientEndColor", gradientEndColor );
            }
        } );

        endColorPicker.setHasAdvanced( true );

        bulkElement.addColorPicker( startColorPicker );
        bulkElement.addColorPicker( endColorPicker );

        subSettings.add( bulkElement );

        // The alpha values
        subSettings.add( new SliderElement( "Gradient Start Alpha", this, new ControlElement.IconData( Material.GLASS ), "gradientStartAlpha", this.gradientStartAlpha ).setRange( 1, 255 ) );
        subSettings.add( new SliderElement( "Gradient End Alpha", this, new ControlElement.IconData( Material.GLASS ), "gradientEndAlpha", this.gradientStartAlpha ).setRange( 1, 255 ) );
    }

    public void loadShader( ResourceLocation resourceLocation ) {
        if ( loadShaderMethod == null )
            loadShaderMethod = ReflectionHelper.findMethod( EntityRenderer.class, Minecraft.getMinecraft().entityRenderer, new String[]{"a", "func_175069_a", "loadShader"}, ResourceLocation.class );

        try {
            loadShaderMethod.invoke( Minecraft.getMinecraft().entityRenderer, resourceLocation );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    public static int getBackgroundColor( final boolean second ) {
        int color = 0;

        if ( second ) {
            int startR = (Blur.getInstance().getGradientStartColor() >> 16) & 0xFF;
            int startG = (Blur.getInstance().getGradientStartColor() >> 8) & 0xFF;
            int startB = (Blur.getInstance().getGradientStartColor() >> 0) & 0xFF;

            color = ModColor.toRGB( startR, startG, startB, Blur.getInstance().getGradientStartAlpha() );
        } else {
            int endR = (Blur.getInstance().getGradientEndColor() >> 16) & 0xFF;
            int endG = (Blur.getInstance().getGradientEndColor() >> 8) & 0xFF;
            int endB = (Blur.getInstance().getGradientEndColor() >> 0) & 0xFF;

            color = ModColor.toRGB( endR, endG, endB, Blur.getInstance().getGradientEndAlpha() );
        }

        float a = color >>> 24;
        float r = color >> 16 & 0xFF;
        float b = color >> 8 & 0xFF;
        float g = color & 0xFF;
        final float prog = Blur.getInstance().getBlurListener().getProgress();
        a *= prog;
        r *= prog;
        g *= prog;
        b *= prog;
        return (( int ) a) << 24 | (( int ) r) << 16 | (( int ) b) << 8 | (( int ) g);
    }

}
